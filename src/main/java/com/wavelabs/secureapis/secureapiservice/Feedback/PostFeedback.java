package com.wavelabs.secureapis.secureapiservice.Feedback;

import java.util.List;

public class PostFeedback {

	private String appId;
	private String eventTag;
	private String groupTag;
	private String questionnaireName;
	private List<Response> responses = null;
	private String transactionId;
	private String userId;
	private Boolean userIdFlag;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getEventTag() {
		return eventTag;
	}

	public void setEventTag(String eventTag) {
		this.eventTag = eventTag;
	}

	public String getGroupTag() {
		return groupTag;
	}

	public void setGroupTag(String groupTag) {
		this.groupTag = groupTag;
	}

	public String getQuestionnaireName() {
		return questionnaireName;
	}

	public void setQuestionnaireName(String questionnaireName) {
		this.questionnaireName = questionnaireName;
	}

	public List<Response> getResponses() {
		return responses;
	}

	public void setResponses(List<Response> responses) {
		this.responses = responses;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Boolean getUserIdFlag() {
		return userIdFlag;
	}

	public void setUserIdFlag(Boolean userIdFlag) {
		this.userIdFlag = userIdFlag;
	}

	@Override
	public String toString() {
		return "PostFeedback [appId=" + appId + ", eventTag=" + eventTag + ", groupTag=" + groupTag
				+ ", questionnaireName=" + questionnaireName + ", responses=" + responses + ", transactionId="
				+ transactionId + ", userId=" + userId + ", userIdFlag=" + userIdFlag + "]";
	}

	
}