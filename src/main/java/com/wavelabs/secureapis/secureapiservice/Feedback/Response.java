package com.wavelabs.secureapis.secureapiservice.Feedback;

import java.util.List;

public class Response {

	private String answerText;
	private String questionId;
	private List<SelectedOption> selectedOptions = null;

	public String getAnswerText() {
		return answerText;
	}

	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public List<SelectedOption> getSelectedOptions() {
		return selectedOptions;
	}

	public void setSelectedOptions(List<SelectedOption> selectedOptions) {
		this.selectedOptions = selectedOptions;
	}

	@Override
	public String toString() {
		return "Response [answerText=" + answerText + ", questionId=" + questionId + ", selectedOptions="
				+ selectedOptions + "]";
	}

	
}