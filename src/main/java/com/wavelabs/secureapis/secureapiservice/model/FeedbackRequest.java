package com.wavelabs.secureapis.secureapiservice.model;

public class FeedbackRequest {

	private String feedbackText;
	private String appLike;
	private String checkAnswer;
	
	public String getFeedbackText() {
		return feedbackText;
	}
	public void setFeedbackText(String feedbackText) {
		this.feedbackText = feedbackText;
	}
	public String getAppLike() {
		return appLike;
	}
	public void setAppLike(String appLike) {
		this.appLike = appLike;
	}
	public String getCheckAnswer() {
		return checkAnswer;
	}
	public void setCheckAnswer(String checkAnswer) {
		this.checkAnswer = checkAnswer;
	}
	@Override
	public String toString() {
		return "FeedbackRequest [feedbackText=" + feedbackText + ", appLike=" + appLike + ", checkAnswer=" + checkAnswer
				+ "]";
	}
}
