package com.wavelabs.secureapis.secureapiservice.model;

public class OptionObj {

	private String optionId;
	private String optionValue;

	public OptionObj(String optionId, String optionValue) {
		super();
		this.optionId = optionId;
		this.optionValue = optionValue;
	}

	public String getOptionId() {
		return optionId;
	}

	public void setOptionId(String optionId) {
		this.optionId = optionId;
	}

	public String getOptionValue() {
		return optionValue;
	}

	public void setOptionValue(String optionValue) {
		this.optionValue = optionValue;
	}
}
