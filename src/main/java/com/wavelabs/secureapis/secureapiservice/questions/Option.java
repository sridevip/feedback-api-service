package com.wavelabs.secureapis.secureapiservice.questions;
public class Option{
	private String id;
	private String text;
	private int order;
	private boolean deleted;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	@Override
	public String toString() {
		return "Option [id=" + id + ", text=" + text + ", order=" + order + ", deleted=" + deleted + "]";
	}
	
	
}