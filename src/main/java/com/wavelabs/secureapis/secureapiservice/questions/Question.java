package com.wavelabs.secureapis.secureapiservice.questions;

import java.util.List;

public class Question{
	private String id;
	private String question;
	private String optionType;
	private List<Option> options;
	private int questionHash;
	private boolean deleted;
	private int order;
	private boolean mandatory;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getOptionType() {
		return optionType;
	}
	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}
	public List<Option> getOptions() {
		return options;
	}
	public void setOptions(List<Option> options) {
		this.options = options;
	}
	public int getQuestionHash() {
		return questionHash;
	}
	public void setQuestionHash(int questionHash) {
		this.questionHash = questionHash;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public boolean isMandatory() {
		return mandatory;
	}
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	@Override
	public String toString() {
		return "Question [id=" + id + ", question=" + question + ", optionType=" + optionType + ", options=" + options
				+ ", questionHash=" + questionHash + ", deleted=" + deleted + ", order=" + order + ", mandatory="
				+ mandatory + "]";
	}
	
	
}