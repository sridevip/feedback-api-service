package com.wavelabs.secureapis.secureapiservice.service;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;

@WebServlet("/loginServlet")
public class CustomServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(CustomServlet.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse resposne) {
		logger.info("API call : " + request.toString());
	}
}
