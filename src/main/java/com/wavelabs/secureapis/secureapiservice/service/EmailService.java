package com.wavelabs.secureapis.secureapiservice.service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.wavelabs.secureapis.secureapiservice.model.Mail;

@Service
public class EmailService {

	@Autowired
	private JavaMailSender emailSender;
	@Autowired
	private MailContentBuilder mailContentBuilder;

	private static final Logger logger = Logger.getLogger(EmailService.class);

	public void sendSimpleMessage(Mail mail) throws MessagingException {
		String mailContent = mailContentBuilder.generateMailContent();
		logger.info("in emai service: ");
		MimeMessage message = emailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		helper.setFrom(mail.getFrom());
		helper.setTo(mail.getTo());
		helper.setSubject("Survey Form:");
		helper.setText(mailContent, true);

		emailSender.send(message);

	}
}
