package com.wavelabs.secureapis.secureapiservice.service;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wavelabs.secureapis.secureapiservice.questions.FeedbackQuestion;

@Service
public class GetFeedbackQuestionsService {

	@Autowired
	RestTemplate restTemplate;

	@Value("${feedback.services.url}")
	private String feedbackUrl;

	private static final Logger logger = Logger.getLogger(GetFeedbackQuestionsService.class);

	public FeedbackQuestion getFeedbackQuestions() {
		logger.info("enter into getFeedbackQuestions service method: ");
		FeedbackQuestion response = restTemplate.getForObject(feedbackUrl, FeedbackQuestion.class);
		ObjectMapper mapper = new ObjectMapper();
		String responseObj = null;
		try {
			responseObj = mapper.writeValueAsString(response);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		logger.info("response from API:  " + responseObj);
		return response;
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
