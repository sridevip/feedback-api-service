package com.wavelabs.secureapis.secureapiservice.service;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wavelabs.secureapis.secureapiservice.Feedback.PostFeedback;
import com.wavelabs.secureapis.secureapiservice.Feedback.Response;
import com.wavelabs.secureapis.secureapiservice.Feedback.SelectedOption;
import com.wavelabs.secureapis.secureapiservice.questions.FeedbackQuestion;

@Service
public class PostFeedbackService {
	@Autowired
	GetFeedbackQuestionsService feedbackQuestions;

	private static final Logger logger = Logger.getLogger(PostFeedbackService.class);

	public String postingFeedack(HttpServletRequest request) {
		Enumeration<String> params = request.getParameterNames();
		FeedbackQuestion feedbackQuestions2 = feedbackQuestions.getFeedbackQuestions();
		PostFeedback postFeedback = new PostFeedback();
		List<Response> responseObj = new ArrayList<>();
		List<SelectedOption> listSelectedOptions = new ArrayList<>();
		postFeedback.setAppId(feedbackQuestions2.getAppId());
		postFeedback.setGroupTag(feedbackQuestions2.getGroupTag());
		postFeedback.setQuestionnaireName(feedbackQuestions2.getQuestionnaireName());
		while (params.hasMoreElements()) {
			String parameter = params.nextElement();
			feedbackQuestions2.getQuestions().stream().forEach(question -> {
				if (parameter.equals(question.getId()) && question.getOptionType().equals("TEXT")) {
					logger.info("in Text: " + request.getParameter(question.getId()));
					Response response = new Response();
					response.setAnswerText(request.getParameter(question.getId()));
					response.setQuestionId(question.getId());
					responseObj.add(response);
				}
				if (parameter.equals(question.getId()) && question.getOptionType().equals("RADIO")) {
					logger.info("in Radio: " + parameter + " :::: " + question.getId());
					Response response = new Response();
					response.setAnswerText(request.getParameter(question.getId()));
					response.setQuestionId(question.getId());
					responseObj.add(response);
					logger.info("after Radion answers added: " + response.toString());
				}
				question.getOptions().stream().forEach(option -> {
					if (parameter.equals(option.getId()) && question.getOptionType().equals("CHECKBOX")) {
						Response response = new Response();
						SelectedOption selectedOption = new SelectedOption();
						response.setAnswerText(request.getParameter(option.getId()));
						response.setQuestionId(question.getId());
						selectedOption.setId(option.getId());
						selectedOption.setText(request.getParameter(option.getId()));
						selectedOption.setOrder(option.getOrder());
						listSelectedOptions.add(selectedOption);
						response.setSelectedOptions(listSelectedOptions);
						responseObj.add(response);
					}
				});
			});
		}

		Set<String> nameSet = new HashSet<>();
		List<Response> newResponse = responseObj.stream().filter(response -> nameSet.add(response.getQuestionId()))
				.collect(Collectors.toList());
		postFeedback.setResponses(newResponse);
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = null;
		try {
			jsonString = mapper.writeValueAsString(postFeedback);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		logger.info("POST feedback Request: " + jsonString);
		return null;
	}

}
